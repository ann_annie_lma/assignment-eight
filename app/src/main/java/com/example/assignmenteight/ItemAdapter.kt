package com.example.assignmenteight

import android.content.ClipData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmenteight.databinding.FirstItemBinding
import com.example.assignmenteight.databinding.SecondItemBinding


interface ItemClick
{
    fun onClick(position:Int)
}

class ItemAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object
    {
        private const val FIRST_ITEM = 1
        private const val SECOND_ITEM = 2
    }

    private val cities = mutableListOf<City>()
    var callback:ItemClick? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if(viewType == FIRST_ITEM)
        {
           FirstViewHolder(
               FirstItemBinding.inflate(
               LayoutInflater.from(parent.context),
               parent,
               false))
        } else
        {
            SecondViewHolder(
                SecondItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is FirstViewHolder)
        {
            holder.bind()
        } else if(holder is SecondViewHolder)
        {
            holder.bind()
        }
    }

    override fun getItemCount(): Int = cities.size

    override fun getItemViewType(position: Int) =
        if(cities[position].imgResource == 0) FIRST_ITEM else SECOND_ITEM

    inner class FirstViewHolder(val binding: FirstItemBinding):
        RecyclerView.ViewHolder(binding.root), View.OnClickListener
    {
        private lateinit var cityModel:City
        fun bind()
        {
            cityModel = cities[adapterPosition]
            binding.tvCityName.text = cityModel.cityName
            binding.tvElevation.text = cityModel.elevation
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callback?.onClick(adapterPosition)
        }
    }

    inner class SecondViewHolder(val binding: SecondItemBinding):
        RecyclerView.ViewHolder(binding.root),View.OnClickListener
    {
        private lateinit var cityModel:City

        fun bind()
        {
            cityModel = cities[adapterPosition]
            binding.tvCityName.text = cityModel.cityName
            binding.tvElevation.text = cityModel.elevation
            binding.imgCity.setImageResource(cityModel.imgResource)
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callback?.onClick(adapterPosition)

        }
    }


    fun setData(cities:MutableList<City>)
    {
        this.cities.clear()
        this.cities.addAll(cities)
        notifyDataSetChanged()
    }

    fun deleteItem(position:Int)
    {
        this.cities.removeAt(position)
        notifyItemRemoved(position)
    }


}