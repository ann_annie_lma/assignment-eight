package com.example.assignmenteight

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.assignmenteight.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val cities = mutableListOf<City>()
    private lateinit var rvAdapter:ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpRecyclerView()
    }


    private fun setUpRecyclerView()
    {
        loadData()

        binding.rvCities.layoutManager = GridLayoutManager(this,2)
        rvAdapter = ItemAdapter()
        binding.rvCities.adapter = rvAdapter

        rvAdapter.callback = object :ItemClick
        {
            override fun onClick(position:Int) {
                rvAdapter.deleteItem(position)
            }

        }

        rvAdapter.setData(cities)

    }

    private fun loadData()
    {
            cities.add(City("Paris","35m",R.drawable.paris))
            cities.add(City("Jakarta","8m"))
            cities.add(City("Seoul","38m",R.drawable.seoul))
            cities.add(City("Lisbon","2m"))
            cities.add(City("Madrid","820m"))
            cities.add(City("Kyoto","971m",R.drawable.kyoto))
            cities.add(City("Chicago","182m",R.drawable.chicago))

    }
}